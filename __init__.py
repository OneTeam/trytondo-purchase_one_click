# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .purchase import *
__all__ = ['register']


def register():
    Pool.register(
		Configuration,
		Purchase,
        module='one_click_for_purchase', type_='model')
    Pool.register(
		OneClickForPurchase,
        module='one_click_for_purchase', type_='wizard')
    Pool.register(
        module='one_click_for_purchase', type_='report')
