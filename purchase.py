# -*- coding: utf-8 -*-
from trytond.wizard import Wizard, StateAction, StateView, StateTransition, \
    Button
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
import logging
from trytond.model import (ModelView, ModelSQL, ModelSingleton, ValueMixin,
    fields)
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)
from trytond.model.workflow import Workflow
from trytond.modules.account.tax import TaxableMixin
from trytond.pyson import Eval
from .exceptions import ErroresGenerales
from trytond.i18n import gettext


logger = logging.getLogger(__name__)


__all__ = ['OneClickForPurchase','Configuration','Purchase']


class Configuration(metaclass=PoolMeta):
    'Purchase Configuration'
    
    __name__ = 'purchase.configuration'
    
    one_click_to_location = fields.Many2One('stock.location', 'Destino de Compra',
                                        domain=[('active','=',True),('type','=','storage')],
                                        help='Destino de Compra para el modulo one_click_for_purchase'
                                        )

class Purchase(metaclass=PoolMeta):
    'Purchase'
    
    __name__ = 'purchase.purchase'
    
    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls._buttons.update({
                'draft_to_done': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'readonly': ~Eval('lines', []),
                    },
        })
    
    @classmethod
    @ModelView.button
    def draft_to_done(cls,purchases):
        '''Lleva las compras (ids) desde el estado borrador hasta finalizados aprobando
        las facturas y movimientos necesarios.'''
        
        Invoice = Pool().get('account.invoice')
        Move = Pool().get('stock.move')
        Configuration = Pool().get('purchase.configuration')
        configuration = Configuration(1)

        if purchases:
            for purchase in purchases:
                if purchase.state != 'draft' or len(purchase.lines) < 1:
                    raise ErroresGenerales(gettext('one_click_for_purchase.msg_noborrador_sinlineas').format(s=purchase.number))
                    return
                if not purchase.invoice_address:
                    raise ErroresGenerales(gettext('one_click_for_purchase.msg_error_direccion').format(p=purchase.id))
                    return
        else:
            raise ErroresGenerales(gettext('one_click_for_purchase.msg_error_ventanas'))
            return

        cls.set_purchase_date(purchases)#pone fecha de compra a los registros que no lo tienen
        cls.quote(purchases)
        cls.confirm(purchases)
        cls.process(purchases)
        Transaction().commit()#se graba la informacion para poder acceder a las facturas y los movimientos
        purchases = cls.browse([x.id for x in purchases])#se vuelve a leer para cargar informacion de la base de datos, verificar si es necesario.
        purchases = [x for x in purchases if (x.state == 'processing' and len(x.lines) > 0) ]
        if len(purchases) <= 0 :
            raise ErroresGenerales(gettext('one_click_for_purchase.msg_fallo_finalizacion'))
            return
        for purchase in purchases:
            invoices = purchase.invoices
            if invoices:
                for invoice in invoices:
                    if not invoice.invoice_date:
                        invoice.invoice_date = purchase.purchase_date
                        Invoice.save([invoice])
                Invoice.validate_invoice(invoices)
                Invoice.post(invoices)
            shipment_returns = purchase.shipment_returns

            if shipment_returns:
                print("shipment_returns")
                for shipment_return in shipment_returns:
                    if shipment_return.from_location != configuration.one_click_to_location:
                        shipment_return.from_location = configuration.one_click_to_location
                    shipment_return.effective_date = purchase.purchase_date
                    shipment_return.wait([shipment_return])
                    shipment_return.assign_try([shipment_return])
                    shipment_return.done([shipment_return])
                    shipment_return.save()
            moves = purchase.moves
            if moves:
                for move in moves:
                    if move.shipment:
                        continue
                    if move.to_location != configuration.one_click_to_location:
                        move.to_location = configuration.one_click_to_location
                    move.effective_date = purchase.purchase_date
                    Move.save([move])
                    Move.do([move])
            cls.process([purchase])


class OneClickForPurchase(Wizard):
    '''Compra en un click.
    
    Procesa una compra en estado de borrador llevandola al estado "procesada",
    creando las Facturas y Movimientos de Stock, además contabiliza la factura
    y cambia el movimiento de stock de la entrada al almacenamiento y finaliza los movimientos de stock.
    '''
    
    __name__ = 'purchase.one_click_for_purchase'
    
    start = StateTransition()
    
    def transition_start(self):
        Purchase = Pool().get('purchase.purchase')
        purchases = Purchase.browse(Transaction().context['active_ids'])
        Purchase.draft_to_done(purchases)
        return 'end'
